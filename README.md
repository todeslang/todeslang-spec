# プログラミング言語Todeslang

## はじめに

Todeslangは実験的なプログラミング言語である。主要なコンセプトは以下の2点である。

1. 既存のコードを書き換えることなしに、末尾に新たなコードを追加することで、既存の機能を変更または拡張することができる。
1. コンティニュエーションを持ち、現代的な制御フローをコンティニュエーションを用いて実現することができる。

## 名前の由来

Todeslangはトデスキングとlanguageの造語である。ただし、[トデスキングとして知られる人物](http://www.todesking.com/)は開発に参加していない。このプログラミング言語が名前を勝手に借りているだけである。

## 文書

プログラミング言語の仕様を除く文書は以下のリポジトリに置かれている。

* https://gitlab.com/todeslang/todeslabg-doc

## 実装

以下の実装がある。

* https://gitlab.com/todeslang/todeslang


## 文法

### Parsing expression grammar

Todeslangのソースコードの文法をparsing expression grammarで示す。

```
Program := Space (Directive DirectiveSeparator)* Directive Space

Space := (! DirectiveSeparator) HorizontalSpace (LineBreak / Empty) HorizontalSpace

HorizontalSpace := (SpaceCharacter / HorizontalTab)*

DirectiveSeparator := HorizontalSpace LineBreak (HorizontalSpace LineBreak)+ HorizontalSpace

Directive := FullDirective / EasyDirective

FullDirective := DirectiveName Space ";" Space (Parameter Space)* Parameter

DirectiveName := "step" / "continuation" / "anchor"

Parameter := ParamaterName Space ":" Space ParameterValue Space ";"

ParameterName := String

EasyDirective := Expression Space ";" Space Expression

String := NominalString / LiteralString / SpecialString

NominalString := (! SpaceCharacter)
	("a"..."z" / "A"..."Z" / "0"..."9" / SpaceCharacter)*
	("a"..."z" / "A"..."Z" / "0"..."9")

LiteralString := "{" ((! "}") VisibleCharacter)+ "}"

SpecialString := "^@" / ^? / "^!" / "^&" / "^$" / "^file" / "^line"

ParameterValue := Expression

Expression := ThenElseOperatorExpression

ThenElseOperatorExpression := ExplicitThenElseOperatorExpression / RightJoinOperatorExpression

ExplicitThenElseOperatorExpression := RightJoinOperatorExpression Space
	"^then" Space
	RightJoinOperatorExpression Space
	"^else" Space
	RightJoinOperatorExpression

RightJoinOperatorExpression := ExplicitRightJoinOperatorExpression / NoJoinOperatorExpression

ExplicitRightJoinOperatorExpression := NoJoinOperatorExpression Space 
	RightJoinOperator Space 
	RightJoinOperatorExpression

RightJoinOperator := "^and" / "^or"

NoJoinOperatorExpression := ExplicitNoJoinOperatorExpression / LeftJoinOperatorExpression

ExplicitNoJoinOperatorExpression := LeftJoinOperatorExpression Space 
	NoJoinOperator Space 
	LeftJoinOperatorExpression

NoJoinOperator := "=" / "<" / "^le" / "^is" / "^pair" / "^mod"

LeftJoinOperatorExpression := ExplicitLeftJoinOperatorExpression / UnaryOperatorExpression

ExplicitLeftJoinOperatorExpression := UnaryOperatorExpression
	(Space LeftJoinOperator Space UnaryOperatorExpression)+

LeftJoinOperator := "+" / "-" / "*" / "/" / "," / "^as" / ">"

UnaryOperatorExpression := ExplicitUnaryOperatorExpression / NonOperatorExpression

ExplicitUnaryOperatorExpression := UnaryOperator Space UnaryOperatorExpression

UnaryOperator := "^not" / "#" / "^broken-pair"

NonOperatorExpression := LiteralExpression
	/ ExpressionInParentheses
	/ ParameterExpression
	/ SpecialExpression
	/ CompoundExpression

LiteralExpression := String

ExpressionInParentheses := "(" Space Expression Space ")"

ParameterExpression := ("$" Space)+ String

SpecialExpression := "%" / "^true" / "^false"

CompoundExpression := NominalCompoundExpression
	/ EnumerativeCompoundExpression

NominalCompoundExpression := "[" Space (Parameter Space)+ "]"

EnumerativeCompoundExpression := "[" Space (Expression Space ";" Space)+ "]"
```

### 構文抽出

ソースコードを構文木に変換することを構文抽出という。構文抽出の過程で、以下の変換が行われる。

#### 簡易ディレクティブ

簡易ディレクティブ (EasyDirective) は、ディレクティブの名称が*step*であり、2個の式が順に*if*引数と*parameters*引数であるようなフルディレクティブ (FullDirective) に変換される。

#### 名称文字列の空白文字

名称文字列 (NominalString) は空白文字 (SpaceCharacter) を含むことができるが、空白文字は除去される。

#### 特殊なリテラル文字列

特殊なリテラル文字列である<i>^@, ^?, ^!, ^&amp;, ^$</i>は、それぞれ、空文字列、キャリッジリターン、改行 (ラインフィード)、水平タブ、閉じブレースを意味する。<i>^file</i>はソースコードのファイル名を意味する。<i>^line</i>は、ソースコードでの行番号を16進法で表記した文字列を意味する。

#### then-else演算子式

then-else演算子式 (ThenElseOperatorExpression) は、*action*引数が*if*文字列リテラルで、演算子の3個の項が順に*if, then, else*引数に置かれた複合式に変換される。

#### 演算子式

演算子式 (OperatorExpression) は複合式 (CompoundExpression) に変換される。2項演算子を持つ式は、*action*引数に演算子の名称、*left*引数に左辺の式、*right*引数に右辺の式が置かれた複合式に変換される。単項演算子を持つ式は、*action*引数に演算子の名称、*main*引数に右辺の式が置かれた複合式に変換される。

演算子の名称は、以下のような文字列である。

<table>
<tr><th>演算子</th><th>名称</th></tr>
<tr><td>^and</td><td>and</td></tr>
<tr><td>^or</td><td>or</td></tr>
<tr><td>=</td><td>equal</td></tr>
<tr><td>&lt;</td><td>less</td></tr>
<tr><td>^le</td><td>lessorequal</td></tr>
<tr><td>^is</td><td>is</td></tr>
<tr><td>^mod</td><td>modulo</td></tr>
<tr><td>^pair</td><td>pair</td></tr>
<tr><td>+</td><td>plus</td></tr>
<tr><td>-</td><td>minus</td></tr>
<tr><td>*</td><td>multiply</td></tr>
<tr><td>/</td><td>divide</td></tr>
<tr><td>,</td><td>append</td></tr>
<tr><td>^as</td><td>as</td></tr>
<tr><td>></td><td>andthen</td></tr>
<tr><td>^not</td><td>not</td></tr>
<tr><td>#</td><td>stringtonumber</td></tr>
<tr><td>^broken-pair</td><td>brokenpair</td></tr>
</table>

#### 複合式

列挙形式の複合式 (EnumerativeCompoundExpression) は名称形式の複合式 (NominalCompoundExpression) に変換される。1引数の列挙形式の複合式は、*action*引数を持つ名称形式の複合式に変換される。同様に、2引数の列挙形式の複合式は、順に*action, main*引数を持つ名称形式の複合式に変換される。3引数の列挙形式の複合式は、順に*action, method, main*引数を持つ名称形式の複合式に変換される。

## 値

Todeslangでは以下の値を使用できる。

* 自然数
* 浮動小数点数
* 文字列
* ブーリアン
* コンティニュエーション
* 終端コンティニュエーション
* カプセル
* ペア
* 不完全ペア

### 自然数

自然数は0以上の整数である。自然数は*plus, minus, multiply, divide, modulo, equal, less, lessorequal*プリミティブ演算を使用できる。これらのプリミティブ演算は、自然数型の*left*引数と*right*引数を取る。

*stringtonumber*プリミティブ演算は、文字列型の*main*引数を取り、自然数を返す。このとき、文字列を16進法の自然数として解釈する。

### 浮動小数点数

浮動小数点数は*plus, minus, multiply, divide, modulo, equal, less, lessorequal*プリミティブ演算を使用できる。これらのプリミティブ演算は、浮動小数点数の*left*引数と*right*引数を取る。

### 文字列

文字列は*equal, less, lessorequal*プリミティブ演算を使用できる。これらのプリミティブ演算は、文字列型の*left*引数と*right*引数を取る。*less*プリミティブ演算と*lessorequal*プリミティブ演算は、文字列を辞書式に比較する。

*append*プリミティブ演算は、文字列型の*left*引数と文字列型の*right*引数を与えられたとき、これらの文字列を結合した文字列を返す。また、*append*プリミティブ演算は、文字列型の*left*引数と、文字列型でない*right*引数を与えられたとき、*right*引数の値を文字列に変換してから、これらの文字列を結合した文字列を返す。この変換は不可逆である場合がある。

### ブーリアン

ブーリアン型は真と偽の2種類の値を持つ。特殊な式である<i>^true</i>と<i>^false</i>は、それぞれブーリアン型の真と偽として評価される。*not*プリミティブ演算は、*main*引数を取り、論理否定を返す。このとき、ブーリアン型でない値は、真として扱われる。

### コンティニュエーション

*releasecontinuation*プリミティブ演算は、コンティニュエーション型の*main*引数を取る。コンティニュエーションは削除される。

### 終端コンティニュエーション

*getterminalcontinuation*プリミティブ演算は、引数を取らず、終端コンティニュエーションを返す。

### カプセル

カプセルは、オブジェクト指向言語におけるオブジェクトに似た値である。カプセルに対して何らかの操作を行うとき、複合式の*action*引数にカプセルを置き、*method*引数に文字列を置く慣習がある。このとき、*method*引数に置かれた文字列をメソッドディスクリプターと呼ぶ。メソッドディスクリプターは、カプセルに対する操作を指定する目的がある。未知のメソッドディスクリプタ―を与えられたときは、何もせずブーリアン型の偽を返すべきである。

カプセル型の値は、カプセルの実体への参照である。カプセルの実体をコピーしたい場合は、明示的にそのためのメソッドを呼び出す必要がある。

### ペア

ペア型の値は、キーとバリューのペアを表現する。*pair*プリミティブ演算は、*left*引数と*right*引数を取り、ペア型の値を返す。

### 不完全ペア

不完全ペア型の値は、キーとバリューのペアのうち、バリューが存在しないものを表現する。*brokenpair*プリミティブ演算は、*main*引数を取り、不完全ペア型の値を返す。

### 型についての演算

それぞれの型は以下のような名称を持つ。型の名称は文字列である。

* number
* float
* string
* boolean
* continuation
* terminalcontinuation
* capsule
* pair
* brokenpair

*as*プリミティブ演算は、*left*引数と*right*引数を取る。*right*引数は型の名称である。*left*引数の値を、*right*引数で指定された型に変換した値を返す。変換に失敗した場合は、偽を返す。自分自身の型への変換は常に成功し、*left*引数の値がそのまま返る。任意の型からブーリアン型への変換は常に成功し、ブーリアン型でない任意の型の値は、ブーリアン型の真に変換される。自然数型と浮動小数点数型、浮動小数点数型と文字列型は相互に変換可能である。任意の型から文字列型への変換は常に成功するが、その変換は不可逆である場合がある。

*is*プリミティブ演算は、*left*引数と*right*引数を取る。*right*引数は型の名称である。*left*引数の値が、*right*引数で指定された型であるかを調べ、真または偽を返す。

## コンテナ

Todeslangで使用できるコンテナは、配列、集合、マップ、オブジェクト、トリビアルコンテナの5種類である。コンテナはいずれもカプセルである。

配列と集合とマップとオブジェクトはイミュータブルである。すなわち、これらのコンテナに対して何らかの変更を行おうとすると、もとのコンテナは変更されず、そのような変更が行われたかのようなコンテナを新たに生成して返す。

トリビアルコンテナはミュータブルである。

### 配列

*Array*プリミティブ演算は、引数を取らず、空の配列を返す。

*append*プリミティブ演算は、*left*引数と*right*引数を取る。*left*引数が配列であるとき、末尾に*right*引数の値を追加した配列を返す。

配列の*Array*メソッドは、引数を取らず、真を返す。

配列の*size*メソッドは、引数を取らず、要素数を自然数型で返す。

配列の*at*メソッドは、自然数型の*main*引数を取り、指定された要素の値を返す。

配列の*replace*メソッドは、自然数型の*at*引数と任意の型のby引数を取り、要素を置き換えたかのような配列を新たに生成して返す。

配列の*popfront*メソッドは、引数を取らず、先頭の要素を削除したかのような配列を新たに生成して返す。

配列の*popback*メソッドは、引数を取らず、末尾の要素を削除したかのような配列を新たに生成して返す。

配列の*tostring*メソッドは、引数を取らず、文字列を返す。このとき、配列の各要素は、0以上100 (16進法) 未満の自然数である必要がある。

配列の*fromstring*メソッドは、文字列型の*main*引数を取り、新しい配列を返す。このとき、配列の各要素は、0以上100 (16進法) 未満の自然数である。

配列の*sort*メソッドは*main*引数を取る。*main*引数は省略可能であり、省略しないならば比較器である必要がある。配列を安定ソートし、新しい配列を返す。

配列の*has*メソッドは*main*引数を取る。指定された要素を含むかどうかをブーリアン型で返す。

配列の*pushfront*メソッドは*main*引数を取る。先頭に要素を追加したかのような配列を新たに生成して返す。

配列の*equal*メソッドは配列の*main*引数を取る。すべての要素が等しいかどうかをブーリアン型で返す。比較には*equal*プリミティブ演算が使われる。

配列の*startswith*メソッドは配列の*main*引数を取る。配列の先頭部分の要素が*main*引数のすべての要素と等しいかどうかをブーリアン型で返す。比較には*equal*プリミティブ演算が使われる。

配列の*endswith*メソッドは配列の*main*引数を取る。配列の末尾部分の要素が*main*引数のすべての要素と等しいかどうかをブーリアン型で返す。比較には*equal*プリミティブ演算が使われる。

配列の*slice*メソッドは自然数型の*offset*引数と*size*引数を取る。これらの引数で指定された部分の配列を返す。

配列の*findindexall*メソッドは配列の*main*引数を取る。*main*引数で与えられた配列を検索し、出現したインデックスをすべて集めた配列を返す。返される配列の各要素は自然数型である。比較には*equal*プリミティブ演算が使われる。

配列の*replaceall*メソッドは配列の*from*引数と*to*引数を取る。*from*引数で与えられた部分列を*to*引数で与えられた配列に置換した配列を返す。比較には*equal*プリミティブ演算が使われる。

配列の*appendall*メソッドは配列の*main*引数を取る。*main*引数の要素をすべて追加した配列を新たに生成して返す。

### 集合

*Set*プリミティブ演算は、空の集合を返す。*main*引数は省略可能であり、省略しないならば比較器である必要がある。*main*引数を省略すると、文字列と自然数に限り比較が行われる。

*append*プリミティブ演算は、*left*引数と*right*引数を取る。*left*引数がセットであるとき、*right*引数の値を追加した集合を返す。

集合の*Set*メソッドは、引数を取らず、真を返す。

集合の*size*メソッドは、引数を取らず、要素数を自然数型で返す。

集合の*has*メソッドは、*main*引数を取り、指定した値を含むかどうかをブーリアン型で返す。

集合の*erase*メソッドは、*main*引数を取り、指定した値を除去したかのようなセットを新たに生成して返す。

集合の*first*メソッドは、引数を取らず、最初の値を返す。最初の値が存在しないならば、偽を返す。

集合の*next*メソッドは、*main*引数を取り、指定された値の直後の値を返す。直後の値が存在しないならば、偽を返す。

集合の*appendall*メソッドは配列の*main*引数を取る。*main*引数の要素をすべて追加した集合を新たに生成して返す。

集合の*toarray*メソッドは引数を取らず、配列を返す。

### マップ

*Map*プリミティブ演算は、空のマップを返す。*main*引数は省略可能であり、省略しないならば比較器である必要がある。*main*引数を省略すると、文字列と自然数に限り比較が行われる。

*append*プリミティブ演算は、*left*引数と*right*引数を取る。*left*引数がマップであり、*right*引数がペアであるとき、マップにキーとバリューのペアを追加したマップを、新たに生成して返す。*left*引数がマップであり、*right*引数が不完全ペアであるとき、マップから指定されたキーを除去したマップを、新たに生成して返す。

マップの*Map*メソッドは、引数を取らず、真を返す。

マップの*size*メソッドは、引数を取らず、要素数を自然数型で返す。

マップの*has*メソッドは、*main*引数を取り、指定したキーが使用されているかどうかをブーリアン型で返す。

マップの*at*メソッドは、*main*引数を取り、指定したキーに対応するバリューを返す。

マップの*first*メソッドは、引数を取らず、最初のキーを返す。最初のキーが存在しないならば、偽を返す。

マップの*next*メソッドは、*main*引数を取り、指定されたキーの直後のキーを返す。直後のキーが存在しないならば、偽を返す。

### オブジェクト

*Object*プリミティブ演算は、空のオブジェクトを返す。

オブジェクトはマップと同じ機能を持つ。唯一の違いは、*Map*メソッドのかわりに*Object*メソッドを持つことである。

マップではなくオブジェクトを使うときは、以下の規約に従うべきである: アルファベットの大文字で始まる文字列をキー、ブーリアン型の真をバリューとするペアは、そのオブジェクトが何であるかを表す。

### トリビアルコンテナ

トリビアルコンテナは、1個の値を記憶するコンテナである。

*Trivialcontainer*プリミティブ演算は、*main*引数を取り、指定された値を保存したトリビアルコンテナを返す。

トリビアルコンテナの*TrivialContainer*メソッドは、引数を取らず、真を返す。

トリビアルコンテナの*get*メソッドは、引数を取らず、保存された値を返す。

トリビアルコンテナの*set*メソッドは、*main*引数を取り、指定された値を保存する。

### 比較器

比較器はコンテナとともに使われることの多い概念である。比較器は、コンティニュエーションのうち、以下の性質を満たすものである。

- コンティニュエーションパラメーターとしてホスト言語から配列を受け取る。この配列は2要素である。
- 配列の第0要素と第1要素を比較する。この比較は推移的かつ非反射的である必要がある。
- 一連の処理の終わりに、終端コンティニュエーションを実行する。このとき、終端コンティニュエーションパラメーターとして、比較の結果をブーリアン型でホスト言語に渡す。

## ホスト言語とゲスト言語

プログラミング言語Aがプログラミング言語Bで実装されているとき、Aをゲスト言語、Bをホスト言語という。この文書では、ゲスト言語はTodeslangである。ホスト言語が何であるかは規定しない。

## ブートストラップ

Todeslangのプログラムの実行を開始すると、*action*引数の値が*main*という文字列である複合式が評価される。この複合式の評価が終わると、プログラムの実行を終了する。

## 制御式

複合式のうち、*action*引数が以下の文字列であるものは、制御式である。制御式でない複合式を一般複合式と呼ぶ。

* and
* or
* if
* returnandthen

*and*制御式は、まず*left*引数を評価する。*left*引数の値が真であれば、*right*引数を評価し、*right*引数の値を返す。*left*引数の値が偽であれば、偽を返す。

*or*制御式は、まず*left*引数を評価する。*left*引数の値が真であれば、真を返す。*left*引数の値が偽であれば、*right*引数を評価し、*right*引数の値を返す。

*if*制御式は、まず*if*引数を評価する。*if*引数の値が真であれば、*then*引数を評価し、*then*引数の値を返す。*if*引数の値が偽であれば、*else*引数を評価し、*else*引数の値を返す。

ここで、「真である」とは、ブーリアン型の真であるか、または、ブーリアン型でない任意の型の値であることを意味する。「偽である」とは、ブーリアン型の偽であることを意味する。

*returnandthen*制御式の評価は、以下の手順で行われる。

1. *left*引数を評価する。
1. *left*引数の値を逐次実行スタックにプッシュする。
1. *right*引数を評価する。
1. 逐次実行スタックをポップする。
1. *right*引数の値を返す。

特殊な式である<i>%</i>は、逐次実行スタックのトップの値を返す。

## 一般複合式

一般複合式の評価は、以下の手順で行われる。

1. すべての引数を評価する。
1. 引数解決を行う。

## 引数解決

引数解決は、0回以上の引数変換と、1回の引数解決ファイナライズから成る過程である。引数変換と引数解決ファイナライズを総称して、引数解決ステップという。

引数解決ステップには、優先順位が高い順に、以下の種類がある。

1. 引数変換ディレクティブ
1. カプセル
1. コンティニュエーション
1. 終端コンティニュエーション
1. プリミティブ演算
1. 最終フォールバック

## 引数変換ディレクティブ

引数解決ステップのうち、引数変換ディレクティブは、引数変換である。引数変換ディレクティブには以下のものがある。

- step引数変換ディレクティブ
- continuation引数変換ディレクティブ
- anchor引数変換ディレクティブ

### step引数変換ディレクティブ

step引数変換ディレクティブは*if*引数と*parameters*引数を取る。*if*引数と*parameters*引数は必須である。*parameters*引数には複合式を記述する必要がある。この複合式の引数を*parameters*引数内引数という。

step引数解決ディレクティブが有効であるかどうかは、以下の規則によって決定される。

1. これまでの引数解決ステップのうちいずれかがanchor引数解決ディレクティブであるとき、そのanchor引数解決ディレクティブよりも前方に記述されている引数解決ディレクティブのみが有効である。
1. ある引数変換ディレクティブの*if*引数を評価するとき、引数解決ディレクティブは無効である。
1. 引数解決ディレクティブの*if*引数の値が真であるとき、その引数解決ディレクティブが有効である。ここで「真である」とは、ブーリアン型の真であるか、または、ブーリアン型でない任意の値であることである。

有効な引数解決ディレクティブのうち、最も後方に記述されているものが、実際に引数変換に使用される。有効な引数解決ディレクティブが存在しないときは、他の引数解決ステップが実行される。

step引数解決ディレクティブが引数変換に使用されるとき、以下のように動作する。複合式の引数のうち、*parameters*引数内引数に同名のものがあれば、その値を置き換える。また、*parameters*引数内引数のうち、複合式の引数に存在しないものがあれば、引数とその値を追加する。

### continuation引数解決ディレクティブ

continuation引数変換ディレクティブは*if, walk, jump*引数を取る。*if, walk, jump*引数はいずれも必須である。*walk*引数と*jump*引数には複合式を記述する必要がある。これらの複合式の引数を、それぞれ、*walk*引数内引数、*jump*引数内引数という。

continuation引数解決ディレクティブが有効であるかどうかは、以下の規則によって決定される。

1. これまでの引数解決ステップのうちいずれかがanchor引数解決ディレクティブであるとき、そのanchor引数解決ディレクティブよりも前方に記述されている引数解決ディレクティブのみが有効である。
1. ある引数変換ディレクティブの*if*引数を評価するとき、引数解決ディレクティブは無効である。
1. 引数解決ディレクティブの*if*引数の値が真であるとき、その引数解決ディレクティブが有効である。ここで「真である」とは、ブーリアン型の真であるか、または、ブーリアン型でない任意の値であることである。

有効な引数解決ディレクティブのうち、最も後方に記述されているものが、実際に引数変換に使用される。有効な引数解決ディレクティブが存在しないときは、他の引数解決ステップが実行される。

continuation引数解決ディレクティブが引数変換に使用されるとき、以下のように動作する。複合式の引数のうち、*walk*引数内引数に同名のものがあれば、その値を置き換える。また、*walk*引数内引数のうち、複合式の引数に存在しないものがあれば、引数とその値を追加する。

このとき、コンティニュエーションが新たに生成される。生成されたコンティニュエーションは、*walk*引数内引数を評価する過程で、*getcontinuation*プリミティブ演算で得ることができる。*getcontinuation*プリミティブ演算は、引数を取らず、コンティニュエーションを返す。

### anchor引数変換ディレクティブ

anchor引数変換ディレクティブは*if*引数と*parameters*引数を取る。*if*引数と*parameters*引数は必須である。*parameters*引数には複合式を記述する必要がある。この複合式の引数を*parameters*引数内引数という。

anchor引数解決ディレクティブが有効であるかどうかは、以下の規則によって決定される。

1. これまでの引数解決ステップのうちいずれかがanchor引数解決ディレクティブであるとき、そのanchor引数解決ディレクティブよりも前方に記述されている引数解決ディレクティブのみが有効である。
1. ある引数変換ディレクティブの*if*引数を評価するとき、引数解決ディレクティブは無効である。
1. 引数解決ディレクティブの*if*引数の値が真であるとき、その引数解決ディレクティブが有効である。ここで「真である」とは、ブーリアン型の真であるか、または、ブーリアン型でない任意の値であることである。

有効な引数解決ディレクティブのうち、最も後方に記述されているものが、実際に引数変換に使用される。有効な引数解決ディレクティブが存在しないときは、他の引数解決ステップが実行される。

anchor引数解決ディレクティブが引数変換に使用されるとき、以下のように動作する。複合式の引数のうち、*parameters*引数内引数に同名のものがあれば、その値を置き換える。また、*parameters*引数内引数のうち、複合式の引数に存在しないものがあれば、引数とその値を追加する。

### 特殊な式

引数解決ディレクティブの引数または引数内引数を評価する過程で、以下の特殊な式を使用できる。

1個の<i>$</i>記号と引数名を組み合わせた式は、現在の複合式の、指定された引数の値を返す。指定された引数が存在しないときは、ブーリアン型の偽を返す。

2個の<i>$</i>記号と引数名を組み合わせた式は、現在の複合式から呼び出し元の複合式に向かって同名の引数を走査し、最初に見つかった偽でない値を返す。偽でない値が見つからなかったときは、ブーリアン型の偽を返す。

3個の<i>$</i>記号と引数名を組み合わせた式は、現在の複合式から呼び出し元の複合式に向かって同名の引数を走査し、それらを順にまとめた配列を返す。

## 引数解決ファイナライズ

引数解決ステップのうち、以下のものは、引数解決ファイナライズである。

- カプセル
- コンティニュエーション
- 終端コンティニュエーション
- プリミティブ演算
- 最終フォールバック

このうちカプセル、プリミティブ演算、最終フォールバックでは、一般複合式の評価が完了し、値が確定する。コンティニュエーションと終端コンティニュエーションでは、一般複合式の評価は放棄される。

### カプセル

*action*引数の値がカプセルであるとき、そのカプセルに付属する処理がホスト言語側で実行される。過剰な引数は無視される。また、省略された引数はブーリアン型の偽とみなされる。戻り値を必要としないメソッドは、ブーリアン型の偽を返す。

### コンティニュエーション

*action*引数の値がコンティニュエーションであるとき、そのコンティニュエーションが実行される。このとき、*main*引数の値は、コンティニュエーションパラメーターとして参照することができる。*main*引数が省略されたときは、ブーリアン型の偽とみなされる。

コンティニュエーションを実行すると、そのコンティニュエーションを生成したcontinuation引数変換ディレクティブの位置から、プログラムの実行を継続する。ただし、*walk*引数内引数ではなく*jump*引数内引数を用いて、複合式の引数を置き換える。*jump*引数内引数を評価する過程では、*getcontinuationparameter*プリミティブ演算によって、コンティニュエーションパラメーターを得ることができる。*getcontinuationparameter*プリミティブ演算は、引数を取らず、コンティニュエーションパラメーターを返す。

### 終端コンティニュエーション

*action*引数の値が終端コンティニュエーションであるとき、ゲスト言語の実行を終了し、ホスト言語に制御を移す。このとき、ゲスト言語での*main*引数の値は、ホスト言語の側で、終端コンティニュエーションパラメーターとして参照することができる。

### プリミティブ演算

*action*引数の値が文字列であり、いずれかのプリミティブ演算の名称と一致するとき、そのプリミティブ演算の処理がホスト言語側で実行される。過剰な引数は無視される。また、省略された引数はブーリアン型の偽とみなされる。戻り値を必要としないプリミティブ演算は、ブーリアン型の偽を返す。

### 最終フォールバック

他のすべての引数解決ステップが無効であるとき、最終フォールバックが行われる。これは、何も処理を実行せず、ブーリアン型の偽を複合式の値として、複合式の評価を完了する。

## 例外

例外を実現するために、以下の機能が用意されている。

*try*プリミティブ演算は*main, continuationparameter, fallback*引数を取る。*main*引数は必須で、コンティニュエーションである必要がある。*continuationparameter*引数と*fallback*引数は省略可能である。*continuationparameter*引数の値はコンティニュエーションに渡される。コンティニュエーションは終端コンティニュエーションで終わる必要がある。終端コンティニュエーションの*main*引数が*try*プリミティブ演算の戻り値である。コンティニュエーションを実行する過程で例外が投げられたときは、*try*プリミティブ演算の戻り値は*fallback*引数の値である。

*throwexception*プリミティブ演算は*main, file, line*引数を取る。*main*引数と*file*引数は省略可能で、省略しないならば文字列である必要がある。*line*引数は省略可能で、省略しないならば自然数である必要がある。このプリミティブ演算は例外を投げる。

## その他のプリミティブ演算

*echo*プリミティブ演算は、*main*引数を取り、*main*引数の値をそのまま返す。

*print*プリミティブ演算は、文字列型の*main*引数を取り、文字列を何らかの装置に出力する。
